import React from 'react';
import './App.css';
import List from './List/List';
import Link from './Link/Link';

function App() {
 const  items=Array.from({length:10},(v,k)=>k+1);
 const onClickHandler=()=>{
   alert('Click function working');
 }
  return (
    <div className="App">
      <Link page="http://www.facebook.com" className="linkname">Facebook</Link>
      <div>
      <button className="btn btn-primary buttonClass"  onClick={onClickHandler}>Click Here</button>
      </div>
     <List items={items}/>
    </div>
  );
}

export default App;
