
import renderer from 'react-test-renderer';
import Link from './Link';
import React from 'react';


describe('<Link/>',()=>{
    it('renders Link correctly', () => {
        const tree = renderer
          .create(<Link page="http://www.facebook.com">Facebook</Link>)
          .toJSON();
        expect(tree).toMatchSnapshot();
      });
});
