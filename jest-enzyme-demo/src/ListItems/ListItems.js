import React from 'react';
import './ListItems.css';

const ListItems = (props) => {
    const {items}= props;
    return (
        <>       
            {items.map((item, index) => <li key={index.toString()} className="item">{item}</li>)}
        </>
    );
};

export default ListItems;