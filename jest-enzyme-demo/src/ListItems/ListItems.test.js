import React from 'react';
import { mount } from 'enzyme';
import ListItems from './ListItems';

describe('<List/>', () => {

    it('get first element of the list', () => {
        let items = [1, 2, 3];
        let wrapper = mount(<ListItems items={items} />);
        expect(wrapper.find('.item').get(0).props.children).toEqual(1);
        wrapper.unmount();
    });
    //snapshot testing example
    test('snapshot testing', () => {
        const data = [1, 2, 3, 4];
        expect(data).toMatchSnapshot();
    });
});