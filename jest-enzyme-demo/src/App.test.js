import React from 'react';
import { shallow, mount } from 'enzyme';
import App from './App';

describe('App component', () => {
  it('to check if p is available', () => {
    const wrapper = shallow(<App />);
    const text = wrapper.find('p');
    expect(text).toHaveLength(0);
  });


  it('Check if <Link/> is working as expected', () => {
    const wrapper = mount(<App />);
    const text = wrapper.find('.linkname');
    expect(text.text()).toMatch(/Face/);
    wrapper.unmount();
  });


  it('button click should open alert', () => {
    let clickFn = jest.fn();
    const wrapper = shallow(<button className="btn btn-primary buttonClass" onClick={clickFn}>Click Here</button>);
    wrapper.find('.buttonClass').simulate('click');
    expect(clickFn).toHaveBeenCalled();
  });

  
  it('button should have certain class', () => {
    let clickFn = jest.fn();
    const wrapper = mount(<button className="btn btn-primary buttonClass" onClick={clickFn}>Click Here</button>);  
    expect(wrapper.exists('.buttonClass')).toEqual(true);
    expect(wrapper.find('.randomClass').exists()).toEqual(false);
  });

});