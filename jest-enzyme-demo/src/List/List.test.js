import React from 'react';
import { shallow} from 'enzyme';
import List from './List';
import ListItems from '../ListItems/ListItems';
//shallow rendering
describe('<List/>', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<List />);
    });

    it('Parent of ListItems is a ul', () => {  
         wrapper = shallow(<List/>);
         wrapper.setProps({items:[1,2]})
        expect(wrapper.find(ListItems).parent().is('ul')).toEqual(true);
    });


    it('renders <ListItems/> when receiving props and its length is greater than 0', () => {  
         wrapper = shallow(<List/>);
         wrapper.setProps({items:[1,2]})
        // Expect the wrapper object to be defined
        expect(wrapper.find(ListItems)).toBeDefined();
        expect(wrapper.find(ListItems)).toHaveLength(1);
    });


    it('renders message when receiving props and its length is 0', () => {
        wrapper.setProps({ items: [] });
        expect(wrapper.contains(<span className="empty-message">No items in list</span>)).toBeTruthy();
    });
});