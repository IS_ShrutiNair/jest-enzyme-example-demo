import React from 'react';
import './List.css';
import ListItems from '../ListItems/ListItems';

const List = (props) => {
    const { items } = props;   
    if (!items.length) {
        // No Items on the list, render an empty message
        return <span className="empty-message">No items in list</span>;
    }

    return (
        <ul className="list-items">
            <ListItems items={items} />
        </ul>
    );
};


List.defaultProps = {
    items: [],
};

export default List;
